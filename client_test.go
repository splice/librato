package librato

import (
	"io"
	"net/http"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

const testURL = "https://metrics-api.librato.com/v1/metrics"

type mockHttp struct {
	mockDo      func(*http.Request) (*http.Response, error)
	forceStatus int
	forceErr    error

	mu    sync.Mutex
	calls int
}

func (m *mockHttp) Do(req *http.Request) (*http.Response, error) {
	m.mu.Lock()
	defer m.mu.Unlock()

	m.calls++
	// If there's a custom func, use it
	if m.mockDo != nil {
		return m.mockDo(req)
	}
	// Return the requested status and error, default to 200, nil
	st := m.forceStatus
	if st == 0 {
		st = 200
	}
	err := m.forceErr
	return &http.Response{StatusCode: st}, err
}

func (m *mockHttp) Count() int {
	m.mu.Lock()
	defer m.mu.Unlock()
	return m.calls
}

func TestClientWithoutAuth(t *testing.T) {
	res, err := http.Get(testURL)
	if assert.Nil(t, err) {
		defer res.Body.Close()
		assert.Equal(t, res.StatusCode, 401)
	}
}

func TestClientInvalidAuth(t *testing.T) {
	ori := DefaultClient.Credentials
	DefaultClient.Credentials = &Credentials{"nogood", "realbad"}
	defer func() {
		DefaultClient.Credentials = ori
	}()
	res, err := Get(testURL)
	if assert.Nil(t, err) {
		defer res.Body.Close()
		assert.Equal(t, res.StatusCode, 401)
	}
}

func TestClientValidAuth(t *testing.T) {
	if DefaultClient.Credentials.User == "" || DefaultClient.Credentials.Token == "" {
		t.Skip("skipping TestClientValidAuth, no credentials defined (set LIBRATO_USER and LIBRATO_TOKEN to test)")
	}
	res, err := Get(testURL)
	if assert.Nil(t, err) {
		defer res.Body.Close()
		assert.Equal(t, res.StatusCode, 200)
	}
}

func TestClientNoRetry(t *testing.T) {
	m := &mockHttp{forceStatus: 503}
	c := &Client{Client: m}
	c.Retries = 0
	res, err := c.Get(testURL)
	if assert.Nil(t, err) {
		assert.Equal(t, m.Count(), 1)
		assert.Equal(t, res.StatusCode, 503)
	}
}

func TestClientRetry(t *testing.T) {
	m := &mockHttp{forceStatus: 503}
	c := &Client{Client: m}
	c.Retries = 3
	res, err := c.Get(testURL)
	if assert.Nil(t, err) {
		assert.Equal(t, m.Count(), 4)
		assert.Equal(t, res.StatusCode, 503)
	}
}

func TestClientRetryDelay(t *testing.T) {
	m := &mockHttp{forceStatus: 503}
	c := &Client{Client: m}
	c.Retries = 2
	c.RetryDelay = 100 * time.Millisecond
	start := time.Now()
	res, err := c.Get(testURL)
	if assert.Nil(t, err) {
		assert.True(t, start.Add(200*time.Millisecond).Before(time.Now()))
		assert.True(t, start.Add(300*time.Millisecond).After(time.Now()))
		assert.Equal(t, m.Count(), 3)
		assert.Equal(t, res.StatusCode, 503)
	}
}

func TestClientError(t *testing.T) {
	m := &mockHttp{forceErr: io.EOF}
	c := &Client{Client: m}
	c.Retries = 2
	_, err := c.Get(testURL)
	assert.Equal(t, err, io.EOF)
	assert.Equal(t, m.Count(), 1)
}

func TestClientRetry200(t *testing.T) {
	m := &mockHttp{}
	c := &Client{Client: m}
	c.Retries = 2
	res, err := c.Get(testURL)
	if assert.Nil(t, err) {
		assert.Equal(t, m.Count(), 1)
		assert.Equal(t, res.StatusCode, 200)
	}
}
