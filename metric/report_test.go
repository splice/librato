package metric

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"sync"
	"testing"
	"time"

	"bitbucket.org/splice/librato"
	"github.com/stretchr/testify/assert"
)

type mockHttp struct {
	mu   sync.Mutex
	cnt  int
	err  error
	nerr int
}

func (m *mockHttp) Do(req *http.Request) (*http.Response, error) {
	m.mu.Lock()
	defer m.mu.Unlock()

	m.cnt++
	if m.nerr > 0 {
		m.nerr--
		e := m.err
		return nil, e
	}
	return &http.Response{StatusCode: 200, Body: ioutil.NopCloser(bytes.NewReader(nil))}, nil
}

func (m *mockHttp) Count() int {
	m.mu.Lock()
	defer m.mu.Unlock()
	return m.cnt
}

func setTempReportPeriod(v time.Duration) func() {
	old := ReportPeriod
	ReportPeriod = v
	return func() {
		ReportPeriod = old
	}
}

func setTempMaxValuesPerSend(v int) func() {
	old := MaxValuesPerSend
	MaxValuesPerSend = v
	return func() {
		MaxValuesPerSend = old
	}
}

func setTempDbgLog(buf *bytes.Buffer) func() {
	var mu sync.Mutex
	old := dbglog
	dbglog = func(f string, args ...interface{}) {
		mu.Lock()
		defer mu.Unlock()
		buf.WriteString(fmt.Sprintf(f, args...))
		buf.WriteString("\n")
	}
	return func() {
		dbglog = old
	}
}

func clearMetrics() {
	metrics = make(map[string]*Metric)
}

func TestSendMetrics(t *testing.T) {
	if librato.DefaultClient.Credentials.User == "" || librato.DefaultClient.Credentials.Token == "" {
		t.Skip("skipping TestSendMetrics, no credentials defined (set LIBRATO_USER and LIBRATO_TOKEN to test)")
	}
	defer clearMetrics()
	ok := true
	Log = func(f string, args ...interface{}) {
		ok = false
	}

	// Register a metric
	m := New(Counter, "test-send", "Test Send Metrics", "", "test")
	// Send a 0-value metric
	m.Add(0)
	sendMetrics(librato.DefaultClient)
	assert.True(t, ok)

	// Add some values
	m.Add(4.5)
	time.Sleep(time.Second)
	m.Add(9.2)
	// Send
	sendMetrics(librato.DefaultClient)
	assert.True(t, ok)

	// Test with multiple metrics
	New(Gauge, "test-empty-metric", "Test Empty Metric", "", "test")
	m2 := New(Gauge, "test-send-2", "Test Send Metrics 2", "", "test")
	m2.Set(34.123)
	time.Sleep(time.Second)
	m2.Set(22)
	time.Sleep(time.Second)
	m2.Add(482.12)
	m.Add(18.12)
	// Send
	sendMetrics(librato.DefaultClient)
	assert.True(t, ok)

	// Test with aggregated gauges
	g1 := New(Gauge, "test-aggregated-gauge", "Test Send Aggregated Gauge Data", "", "test")
	g1.Set(1.2)
	g1.Set(2.3)
	g1.Set(3.4)
	// Send
	sendMetrics(librato.DefaultClient)
	assert.True(t, ok)
}

func TestReportMetricsNoValue(t *testing.T) {
	defer clearMetrics()

	mock := &mockHttp{}
	cli := &librato.Client{Client: mock}
	sendMetrics(cli)
	assert.Equal(t, mock.Count(), 0)
}

func TestReportMetricsWithValues(t *testing.T) {
	defer clearMetrics()

	mock := &mockHttp{}
	cli := &librato.Client{Client: mock}
	met := New(Counter, "test", "", "", "test")
	met.Add(12)
	sendMetrics(cli)
	assert.Equal(t, mock.Count(), 1)
	// Add more values
	met.Add(45)
	sendMetrics(cli)
	assert.Equal(t, mock.Count(), 2)
	// Then without new values, should not increment the count
	sendMetrics(cli)
	assert.Equal(t, mock.Count(), 2)
}

func TestReportMetricsTwice(t *testing.T) {
	defer clearMetrics()

	// Set a long period so that it doesn't report during tests
	defer setTempReportPeriod(24 * time.Hour)()

	// Set the debug log func
	var buf bytes.Buffer
	defer setTempDbgLog(&buf)()

	mock := &mockHttp{}
	StartReporting(&librato.Client{Client: mock})
	time.Sleep(100 * time.Millisecond)
	StartReporting(&librato.Client{Client: mock})
	time.Sleep(100 * time.Millisecond)
	got := buf.String()
	exp := "metric: report goroutine started\n"
	if got != exp {
		t.Errorf("expected %s, got %s", exp, got)
	}
}

func TestReportOneMetricNoSplit(t *testing.T) {
	defer clearMetrics()

	defer setTempMaxValuesPerSend(2)()
	var buf bytes.Buffer
	defer setTempDbgLog(&buf)()

	mock := &mockHttp{}
	cli := &librato.Client{Client: mock}
	met := New(Counter, "test", "", "", "test")

	// Try with a single value
	met.Add(12)
	sendMetrics(cli)
	assert.Equal(t, mock.Count(), 1)
	assert.Equal(t, buf.String(), "metric: sendToLibrato: 1 of 1\n")
	buf.Reset()

	// With two values
	met.Add(45)
	time.Sleep(time.Second)
	met.Add(46)
	sendMetrics(cli)
	assert.Equal(t, mock.Count(), 2)
	assert.Equal(t, buf.String(), "metric: sendToLibrato: 1 of 1\n")
	buf.Reset()

	// With three values, but on same metric - will NOT split
	met.Add(13)
	time.Sleep(time.Second)
	met.Add(45)
	time.Sleep(time.Second)
	met.Add(67)
	sendMetrics(cli)
	assert.Equal(t, mock.Count(), 3)
	assert.Equal(t, buf.String(), "metric: sendToLibrato: 1 of 1\n")
	buf.Reset()

	// Then without new values, should not increment the count
	sendMetrics(cli)
	assert.Equal(t, mock.Count(), 3)
	assert.Equal(t, buf.String(), "")
}

func TestReportMetricsSplit(t *testing.T) {
	defer clearMetrics()

	defer setTempMaxValuesPerSend(2)()
	var buf bytes.Buffer
	defer setTempDbgLog(&buf)()

	mock := &mockHttp{}
	cli := &librato.Client{Client: mock}
	m1 := New(Counter, "test-1", "", "", "test")
	m2 := New(Counter, "test-2", "", "", "test")

	// One value each, no split
	m1.Add(1)
	m2.Add(2)
	sendMetrics(cli)
	assert.Equal(t, mock.Count(), 1)
	assert.Equal(t, buf.String(), "metric: sendToLibrato: 1 of 1\n")
	assert.Equal(t, len(m1.vals), 0)
	assert.Equal(t, len(m1.oldVals), 0)
	assert.Equal(t, len(m2.vals), 0)
	assert.Equal(t, len(m2.oldVals), 0)
	buf.Reset()

	// Two values in one, one in other, split in 2 calls
	m1.Add(1)
	time.Sleep(time.Second)
	m1.Add(1)
	m2.Add(2)
	sendMetrics(cli)
	assert.Equal(t, mock.Count(), 3)
	assert.Equal(t, buf.String(), "metric: sendToLibrato: 1 of 2\nmetric: sendToLibrato: 2 of 2\n")
	assert.Equal(t, len(m1.vals), 0)
	assert.Equal(t, len(m1.oldVals), 0)
	assert.Equal(t, len(m2.vals), 0)
	assert.Equal(t, len(m2.oldVals), 0)
	buf.Reset()

	// Then without new values, should not increment the count
	sendMetrics(cli)
	assert.Equal(t, mock.Count(), 3)
	assert.Equal(t, buf.String(), "")
}

func TestSendToLibratoRetry(t *testing.T) {
	defer clearMetrics()
	var buf bytes.Buffer
	defer setTempDbgLog(&buf)()

	mock := &mockHttp{err: io.EOF, nerr: 1} // simulate an error on first call
	cli := &librato.Client{Client: mock}
	m1 := New(Counter, "test-1", "", "", "test")

	// First call that fails
	m1.Add(1)
	sendMetrics(cli)
	assert.Equal(t, mock.Count(), 1)
	assert.Equal(t, buf.String(), "metric: sendToLibrato: 1 of 1\n")
	assert.Equal(t, len(m1.vals), 0)
	assert.Equal(t, len(m1.oldVals), 1)
	buf.Reset()

	// Second call succeeds
	sendMetrics(cli)
	assert.Equal(t, mock.Count(), 2)
	assert.Equal(t, buf.String(), "metric: sendToLibrato: 1 of 1\n")
	assert.Equal(t, len(m1.vals), 0)
	assert.Equal(t, len(m1.oldVals), 0)
	buf.Reset()
}

func TestSendToLibratoDrop(t *testing.T) {
	defer clearMetrics()

	mock := &mockHttp{err: io.EOF, nerr: 2} // simulate an error on first two calls
	cli := &librato.Client{Client: mock}
	m1 := New(Counter, "test-1", "", "", "test")

	// Add the value, will fail and move the value to oldval
	m1.Add(1)
	sendMetrics(cli)
	assert.Equal(t, mock.Count(), 1)
	assert.Equal(t, len(m1.vals), 0)
	assert.Equal(t, len(m1.oldVals), 1)

	// Try again, will fail again and this time clear oldVals (because no new vals)
	sendMetrics(cli)
	assert.Equal(t, mock.Count(), 2)
	assert.Equal(t, len(m1.vals), 0)
	assert.Equal(t, len(m1.oldVals), 0)

	// Try again, this time no call to http
	sendMetrics(cli)
	assert.Equal(t, mock.Count(), 2)
}
