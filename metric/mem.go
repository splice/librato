package metric

import (
	"fmt"
	"runtime"
	"sync"
	"time"
)

var (
	// onceMem ensures the memory collection goroutine is started only once.
	onceMem sync.Once
)

// memMetrics holds the metrics for all MemStats.
type memMetrics struct {
	Alloc      *Metric
	TotalAlloc *Metric
	Sys        *Metric
	Lookups    *Metric
	Mallocs    *Metric
	Frees      *Metric

	HeapAlloc    *Metric
	HeapSys      *Metric
	HeapIdle     *Metric
	HeapInuse    *Metric
	HeapReleased *Metric
	HeapObjects  *Metric

	StackInuse  *Metric
	StackSys    *Metric
	MSpanInuse  *Metric
	MSpanSys    *Metric
	MCacheInuse *Metric
	MCacheSys   *Metric
	BuckHashSys *Metric
	GCSys       *Metric
	OtherSys    *Metric

	NextGC       *Metric
	LastGC       *Metric
	PauseTotalNs *Metric
	NumGC        *Metric
}

// Memory starts a goroutine that will collect all memory statistics at regular
// intervals, specified by readFreq. The memory collected is that reported by
// Go's runtime.ReadMemStats function.
//
// It is a no-op if readFreq is 0 or if it has already been called.
func Memory(readFreq time.Duration, namePrefix string, env string) {
	if readFreq <= 0 {
		return
	}
	onceMem.Do(func() {
		go collect(readFreq, namePrefix, env)
	})
}

// collect registers the metrics and reads the memory statistics
// at the specified frequency.
func collect(freq time.Duration, prefix, env string) {
	dbglog("metric: collect goroutine started")
	var m memMetrics
	m.register(prefix, env)
	tick := time.Tick(freq)
	for _ = range tick {
		m.readStats()
	}
}

// readStats reads the runtime memory statistics, and sets the corresponding metrics
// values.
func (m *memMetrics) readStats() {
	var ms runtime.MemStats
	runtime.ReadMemStats(&ms)

	m.Alloc.Set(float64(ms.Alloc))
	m.TotalAlloc.Set(float64(ms.TotalAlloc))
	m.Sys.Set(float64(ms.Sys))
	m.Lookups.Set(float64(ms.Lookups))
	m.Mallocs.Set(float64(ms.Mallocs))
	m.Frees.Set(float64(ms.Frees))

	m.HeapAlloc.Set(float64(ms.HeapAlloc))
	m.HeapSys.Set(float64(ms.HeapSys))
	m.HeapIdle.Set(float64(ms.HeapIdle))
	m.HeapInuse.Set(float64(ms.HeapInuse))
	m.HeapReleased.Set(float64(ms.HeapReleased))
	m.HeapObjects.Set(float64(ms.HeapObjects))

	m.StackInuse.Set(float64(ms.StackInuse))
	m.StackSys.Set(float64(ms.StackSys))
	m.MSpanInuse.Set(float64(ms.MSpanInuse))
	m.MSpanSys.Set(float64(ms.MSpanSys))
	m.MCacheInuse.Set(float64(ms.MCacheInuse))
	m.MCacheSys.Set(float64(ms.MCacheSys))
	m.BuckHashSys.Set(float64(ms.BuckHashSys))
	m.GCSys.Set(float64(ms.GCSys))
	m.OtherSys.Set(float64(ms.OtherSys))

	m.NextGC.Set(float64(ms.NextGC))
	m.LastGC.Set(float64(ms.LastGC))
	m.PauseTotalNs.Set(float64(ms.PauseTotalNs))
	m.NumGC.Set(float64(ms.NumGC))
}

// register creates the Metrics for each memory statistics.
func (m *memMetrics) register(prefix, env string) {
	m.Alloc = New(Gauge, prefix+".memory.alloc", fmt.Sprintf("Alloc (%s)", prefix), "bytes allocated and still in use", env)
	m.TotalAlloc = New(Counter, prefix+".memory.totalalloc", fmt.Sprintf("TotalAlloc (%s)", prefix), "bytes allocated (even if freed)", env)
	m.Sys = New(Counter, prefix+".memory.sys", fmt.Sprintf("Sys (%s)", prefix), "bytes obtained from system (sum of XxxSys)", env)
	m.Lookups = New(Counter, prefix+".memory.lookups", fmt.Sprintf("Lookups (%s)", prefix), "number of pointer lookups", env)
	m.Mallocs = New(Counter, prefix+".memory.mallocs", fmt.Sprintf("Mallocs (%s)", prefix), "number of mallocs", env)
	m.Frees = New(Counter, prefix+".memory.frees", fmt.Sprintf("Frees (%s)", prefix), "number of frees", env)

	m.HeapAlloc = New(Gauge, prefix+".memory.heapalloc", fmt.Sprintf("HeapAlloc (%s)", prefix), "bytes allocated and still in use", env)
	m.HeapSys = New(Counter, prefix+".memory.heapsys", fmt.Sprintf("HeapSys (%s)", prefix), "bytes obtained from system", env)
	m.HeapIdle = New(Gauge, prefix+".memory.heapidle", fmt.Sprintf("HeapIdle (%s)", prefix), "bytes in idle spans", env)
	m.HeapInuse = New(Gauge, prefix+".memory.heapinuse", fmt.Sprintf("HeapInuse (%s)", prefix), "bytes in non-idle span", env)
	m.HeapReleased = New(Counter, prefix+".memory.heapreleased", fmt.Sprintf("HeapReleased (%s)", prefix), "bytes released to the OS", env)
	m.HeapObjects = New(Gauge, prefix+".memory.heapobjects", fmt.Sprintf("HeapObjects (%s)", prefix), "total number of allocated objects", env)

	m.StackInuse = New(Gauge, prefix+".memory.stackinuse", fmt.Sprintf("StackInuse (%s)", prefix), "bootstrap stacks bytes used now", env)
	m.StackSys = New(Counter, prefix+".memory.stacksys", fmt.Sprintf("StackSys (%s)", prefix), "bootstrap stacks bytes obtained from system", env)
	m.MSpanInuse = New(Gauge, prefix+".memory.mspaninuse", fmt.Sprintf("MSpanInuse (%s)", prefix), "mspan structures bytes used now", env)
	m.MSpanSys = New(Counter, prefix+".memory.mspansys", fmt.Sprintf("MSpanSys (%s)", prefix), "mspan structures bytes obtained from system", env)
	m.MCacheInuse = New(Gauge, prefix+".memory.mcacheinuse", fmt.Sprintf("MCacheInuse (%s)", prefix), "mcache structures bytes used now", env)
	m.MCacheSys = New(Counter, prefix+".memory.mcachesys", fmt.Sprintf("MCacheSys (%s)", prefix), "mcache structures bytes obtained from system", env)
	m.BuckHashSys = New(Counter, prefix+".memory.buckhashsys", fmt.Sprintf("BuckHashSys (%s)", prefix), "profiling bucket hash table", env)
	m.GCSys = New(Gauge, prefix+".memory.gcsys", fmt.Sprintf("GCSys (%s)", prefix), "GC metadata", env)
	m.OtherSys = New(Counter, prefix+".memory.othersys", fmt.Sprintf("OtherSys (%s)", prefix), "other system allocations", env)

	m.NextGC = New(Gauge, prefix+".memory.nextgc", fmt.Sprintf("NextGC (%s)", prefix), "next run in HeapAlloc time (bytes)", env)
	m.LastGC = New(Gauge, prefix+".memory.lastgc", fmt.Sprintf("LastGC (%s)", prefix), "last run in absolute time (ns)", env)
	m.PauseTotalNs = New(Counter, prefix+".memory.pausetotalns", fmt.Sprintf("PauseTotalNs (%s)", prefix), "total GC pause time (ns)", env)
	m.NumGC = New(Counter, prefix+".memory.numgc", fmt.Sprintf("NumGC (%s)", prefix), "number of GC runs", env)
}
