// Package metric offers an expvar-like API to register Metrics to collect and report
// to Librato.
//
// Metrics are created with metric.New and are automatically registered in the global metrics map.
// Once metric.StartReporting is called, values will be reported to Librato at the
// metric.ReportPeriod frequency.
//
// The Name and Source of a metric have constraints regarding the length and the accepted
// characters. Metrics created via New automatically have normalized names and sources.
// To assign values directly to the Name or Source fields of a Metric, the NormalizeName
// and NormalizeSource functions can be used to guarantee valid values.
//
package metric

import (
	"fmt"
	"math"
	"os"
	"sync"
	"time"
)

// TODO : Refactor if we want to open-source eventually: Metric struct should not be exposed, makes
// no sense to use literal notation, internal fields must be created. New should probably return
// an interface, and we could have gauge- and counter- specific implementations (so that we don't
// end up with a map[int64][]float64 for a counter, as we do now). And since New automatically registers,
// Register is not required, nor NormalizeXx.

const (
	// Valid metric source and name characters
	nameChars = `-_.:0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz`
	// Maximum metric source and name length
	maxName = 255
)

var (
	// The lock and map holding all registered metrics.
	lock    sync.RWMutex
	metrics = make(map[string]*Metric)

	// Strictly for tests, force a specific timestamp
	testForceKey int64
)

// MetricType represents the type of a Metric.
type MetricType int

const (
	// The supported metric types.
	Counter MetricType = iota
	Gauge
	maxMetricType // marker for loops on supported types
)

var (
	// typeKey is a lookup slice that indicates the JSON key to use for the MetricType.
	typeKey = []string{
		Counter: "counters",
		Gauge:   "gauges",
	}
)

// Metric represents a single Librato metric. A single metric may hold multiple values,
// each saved along with their timestamp. Once reported to Librato, the values are reset.
type Metric struct {
	Name        string
	DisplayName string
	Description string
	Source      string
	Period      time.Duration
	Type        MetricType

	// mu protects the following private fields
	mu           sync.Mutex
	vals         map[int64][]float64
	oldVals      map[int64][]float64
	last         float64
	reportedOnce bool
}

// Add saves a new value for this Metric by adding delta to the last saved value (or 0).
// If the Metric is a counter, the value may replace the existing value for the current Unix timestamp.
// If the Metric is a gauge, the values for a given Unix timestamp are all collected, and the
// aggregates will be sent to Librato.
func (m *Metric) Add(delta float64) {
	if m == nil {
		return
	}
	m.mu.Lock()
	defer m.mu.Unlock()

	m.addValue(m.last + delta)
}

// Set saves v as a new value for this Metric. For counters, it may replace the existing value if the Unix
// timestamp is the same (same second). For gauges, the values for a given Unix timestamp are all collected, and the
// aggregates will be sent to Librato.
func (m *Metric) Set(v float64) {
	if m == nil {
		return
	}
	m.mu.Lock()
	defer m.mu.Unlock()

	m.addValue(v)
}

func (m *Metric) addValue(v float64) {
	if m == nil {
		return
	}
	// Update last value
	m.last = v
	// Compute current timestamp key
	key := time.Now().Unix()
	if testForceKey != 0 {
		key = testForceKey
		// Always reset on each value added
		testForceKey = 0
	}

	// If a value already exists for this key, replace for counters, append
	// for gauges.
	if v, ok := m.vals[key]; ok {
		if m.Type == Counter {
			v[0] = m.last
		} else {
			v = append(v, m.last)
		}
		m.vals[key] = v
		return
	}
	m.vals[key] = []float64{m.last}
}

// New creates a new metric with the given name, display name and description.
// If the metric does not exist, it will be created the first time it is reported to Librato.
// The Source of the metric is set to the current hostname by default, and the Period
// is left at 0. When the metric is reported, the Period is set to the ReportPeriod package
// variable's value if it is 0, which is the correct value in normal cases.
//
// The Metric created with New is automatically registered.
func New(typ MetricType, nm, display, desc, env string) *Metric {
	// If Hostname fails, leave source blank
	host, _ := os.Hostname()
	host = env + "-" + host
	m := &Metric{
		Name:        NormalizeName(nm),
		DisplayName: display,
		Description: desc,
		Source:      NormalizeSource(host),
		Type:        typ,
		vals:        make(map[int64][]float64),
	}
	Register(m)
	return m
}

// Register registers the provided Metric so that it is collected and reported to Librato.
// It should be called in a package's init function, as it may panic if a Metric with the same
// name has already been registered.
func Register(m *Metric) {
	lock.Lock()
	defer lock.Unlock()
	if _, ok := metrics[m.Name]; ok {
		panic(fmt.Sprintf("metric: name %s already registered", m.Name))
	}
	metrics[m.Name] = m
}

// Do calls f for each registered metric. The global metric map is locked during
// execution, but existing metric values may still be collected.
func Do(f func(*Metric)) {
	lock.RLock()
	defer lock.RUnlock()
	for _, m := range metrics {
		f(m)
	}
}

// metricValue represents a JSON-encodable single value of a metric.
type metricValue struct {
	Name        string `json:"name"`
	Source      string `json:"source,omitempty"`
	DisplayName string `json:"display_name,omitempty"`
	Description string `json:"description,omitempty"`
	Period      int    `json:"period,omitempty"`
	MeasureTime int64  `json:"measure_time"`

	// Value is a pointer because when Count is set, Value must not be set, and vice-versa.
	// Since 0 is a valid value to report, cannot set omitempty on a float64. Hence the pointer.
	Value *float64 `json:"value,omitempty"`

	// Gauge-specific parameters, Value should not be set if those are set
	Count      int64   `json:"count,omitempty"`
	Sum        float64 `json:"sum,omitempty"`
	Max        float64 `json:"max,omitempty"`
	Min        float64 `json:"min,omitempty"`
	SumSquares float64 `json:"sum_squares,omitempty"`
}

// flatten returns an array of this Metric's values, ready to be JSON-encoded
// and reported to Librato. It sends values found in both vals and oldVals fields.
//
// On completion, the vals map is moved to oldVals, replacing the existing oldVals,
// and the vals map is reset.
//
// If the reporter successfully sends the values to Librato, then this metric's
// clear method will be called, and oldVals will be cleared. If the call to Librato
// fails, then on the next report attempt, the oldVals will be sent a second time along
// with the new vals.
//
// This ensures two things: new values that may be collected between the calls to
// flatten and clear are not dropped (they are collected in m.vals, and on clear only
// m.oldVals is cleared), and values don't get accumulated indefinitely if the calls
// to Librato fail multiple times. Values get at most 2 chances to be sent to Librato,
// after which they are dropped.
//
func (m *Metric) flatten() []*metricValue {
	if m == nil {
		return nil
	}

	m.mu.Lock()
	defer m.mu.Unlock()

	// Check if there's anything to report
	if len(m.oldVals)+len(m.vals) == 0 {
		return nil
	}

	// There is, create the data slice to hold all values.
	data := make([]*metricValue, len(m.oldVals)+len(m.vals))
	i := 0
	// The func to add a single value to the data slice
	addValue := func(t int64, v []float64) {
		mv := &metricValue{
			Name:        m.Name,
			Source:      m.Source,
			MeasureTime: t,
		}
		var val float64
		switch m.Type {
		case Counter:
			val = v[0]
			mv.Value = &val
		case Gauge:
			if len(v) == 1 {
				// Only one value, just send it and be done with it
				val = v[0]
				mv.Value = &val
				break
			}
			// Compute count, sum, min, max and sumsquares
			mv.Count = int64(len(v))
			mv.Min, mv.Max, mv.Sum, mv.SumSquares = aggregates(v)
		}
		// If the Metric has never been reported yet, add the optional parameters, so
		// that if Librato creates the metric, it has the expected values (ignored by Librato
		// on subsequent calls).
		if !m.reportedOnce {
			mv.DisplayName = m.DisplayName
			mv.Description = m.Description
			// Unless a Period is specifically set on the Metric, use the ReportPeriod value.
			per := int(m.Period.Seconds())
			if per == 0 {
				per = int(ReportPeriod.Seconds())
			}
			mv.Period = per
		}
		data[i] = mv
		i++
	}
	// Add oldVals
	for t, v := range m.oldVals {
		addValue(t, v)
	}
	// Add new vals
	for t, v := range m.vals {
		addValue(t, v)
	}

	// Move new vals to oldVals, reset new vals
	m.oldVals = m.vals
	m.vals = make(map[int64][]float64)

	return data
}

func aggregates(vals []float64) (min, max, sum, sumsq float64) {
	min = math.Inf(1)
	max = math.Inf(-1)
	for _, v := range vals {
		min = math.Min(min, v)
		max = math.Max(max, v)
		sum += v
		sumsq += v * v
	}
	return min, max, sum, sumsq
}

// clear removes all oldVals from the Metric. It keeps the last value
// so that subsequent calls to Add(delta) do the right thing, and the vals
// map because those values are those added since the call to flatten.
func (m *Metric) clear() {
	if m == nil {
		return
	}

	m.mu.Lock()
	defer m.mu.Unlock()
	m.oldVals = nil
	// Set as reported once only on a call to clear, meaning a send has been successful.
	m.reportedOnce = true
}

// NormalizeName returns a Librato-compatible name.
func NormalizeName(nm string) string {
	return normalize(nm)
}

// NormalizeSource returns a Librato-compatible source.
func NormalizeSource(src string) string {
	return normalize(src)
}

// normalize takes a string and makes it Librato-compatible. Librato metric sources and
// names are limited to 255 characters, and may consist only of 'A-Za-z0-9.:-_'.
func normalize(s string) string {
	if len(s) > maxName {
		s = s[:maxName]
	}
	cleaned := make([]byte, len(s))

	ix := -1
outer:
	for _, c := range s {
		ix++
		for _, m := range nameChars {
			if c == m {
				cleaned[ix] = byte(c)
				continue outer
			}
		}
		cleaned[ix] = '-'
	}
	return string(cleaned)
}
