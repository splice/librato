package metric

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"sync"
	"time"

	"bitbucket.org/splice/librato"
)

var (
	// ReportPeriod is the frequency at which registered metrics are reported
	// to Librato.
	ReportPeriod time.Duration

	// MaxValuesPerSend is the maximum number of values to send to Librato in a
	// single call. Librato recommends 300 values maximum per call, which is the
	// default value.
	//
	// Note that the granularity is per metric, and the send is executed once the
	// limit is reached, so it may result in a send with more than this number of values.
	//
	// Since a metric cannot have more than one value per second (because a value
	// has a Unix timestamp as key), and values that could not be reported at the
	// last attempt will be reported again on the next attempt, for a ReportPeriod
	// of 5 minutes (300 seconds), a single metric may have up to 600 values.
	MaxValuesPerSend int = 300

	// Log is the logging function to use to report errors during metric reporting.
	// It defaults to the stdlib's log.Printf function. Setting it to nil disables
	// logging.
	Log func(string, ...interface{}) = log.Printf

	// dbglog is for debugging and testing purposes and is a no-op by default.
	dbglog func(string, ...interface{}) = func(fmt string, args ...interface{}) {}

	// once ensures the reporting goroutine can only be started once.
	once sync.Once
)

// StartReporting starts a goroutine that reports the metrics at the ReportPeriod
// frequency. It is a no-op if ReportPeriod is 0 or if the reporting goroutine
// is already started.
//
// The client is the librato client to use to call the Librato API. If it is
// nil, librato.DefaultClient is used.
func StartReporting(client *librato.Client) {
	if ReportPeriod <= 0 {
		return
	}
	if client == nil {
		client = librato.DefaultClient
	}
	// Run the goroutine only once, no-op on subsequent calls.
	once.Do(func() {
		go report(client)
	})
}

// report runs in a separate goroutine and sends metrics to Librato at the defined
// interval.
func report(c *librato.Client) {
	dbglog("metric: report goroutine started")
	tick := time.Tick(ReportPeriod)
	for _ = range tick {
		sendMetrics(c)
	}
}

// call represents a single call to Librato, it holds the metrics involved
// and the values to send, as well as a counter used to split the calls.
type call struct {
	metrics []*Metric
	vals    map[MetricType][]*metricValue
	cnt     int
}

// sendMetrics collects all values from all registered metrics, and sends them
// to Librato.
//
// Values are split based on MaxValuesPerSend. On success, the metric's clear
// method is called. On error, the error is logged, and the metric's values are
// kept in memory to be sent again on the next attempt, before getting dropped if
// the send fails again. See the documentation on Metric.flatten for more details.
func sendMetrics(c *librato.Client) {
	// Gather values, split in separate calls as required
	var calls []*call
	Do(func(m *Metric) {
		// Ignore unknown/unsupported metric types
		if m.Type >= 0 && m.Type < maxMetricType {
			vals := m.flatten()
			if len(vals) == 0 {
				return
			}
			if l := len(calls); l == 0 || calls[l-1].cnt >= MaxValuesPerSend {
				// New call
				calls = append(calls, &call{
					vals: make(map[MetricType][]*metricValue),
				})
			}
			cur := calls[len(calls)-1]
			cur.metrics = append(cur.metrics, m)
			cur.vals[m.Type] = append(cur.vals[m.Type], vals...)
			cur.cnt += len(vals)
		}
	})

	// Do we have something to report?
	if len(calls) == 0 {
		//Log("metric: sendMetrics: nothing to send")
		return
	}

	// Yes, send in parallel, clear metrics based on which one got through.
	var wg sync.WaitGroup
	wg.Add(len(calls))
	for i, call := range calls {
		go sendToLibrato(c, call, &wg, i, len(calls))
	}
	// Do not return until all calls were completed, so that no other sendMetrics
	// call can happen while the previous one is still running.
	wg.Wait()
}

// sendToLibrato actually sends the JSON-encoded values to Librato. On success, it clears
// the values from the metrics related to this call.
func sendToLibrato(cli *librato.Client, call *call, wg *sync.WaitGroup, ix, cnt int) {
	defer wg.Done()

	dbglog("metric: sendToLibrato: %d of %d", ix+1, cnt)

	// Marshal to JSON and send
	data := make(map[string]interface{})
	for k, v := range call.vals {
		data[typeKey[k]] = v
	}
	b, err := json.Marshal(data)
	if err != nil {
		Log("metric: sendToLibrato: error marshaling to json: %s", err)
		return
	}

	// POST data to Librato
	Log("metric: sendToLibrato: librato data to push (%d of %d): %d", ix+1, cnt, len(b))
	res, err := cli.Post("https://metrics-api.librato.com/v1/metrics", "application/json", bytes.NewReader(b))
	if err != nil {
		Log("metric: sendToLibrato: error POST: %s", err)
		return
	}
	defer res.Body.Close()

	// Check status code
	if res.StatusCode < 200 || res.StatusCode >= 300 {
		// Try to read the body, Librato's error reporting is quite good
		b, err = ioutil.ReadAll(res.Body)
		if err == nil {
			Log("metric: non-success status code: %d (%s)\n%s", res.StatusCode, res.Status, string(b))
			return
		}
		Log("metric: non-success status code: %d (%s)", res.StatusCode, res.Status)
		return
	}

	// Success, clear the metrics' old values
	for _, m := range call.metrics {
		m.clear()
	}
	Log(fmt.Sprintf("metric: sendToLibrato %d of %d: success", ix+1, cnt))
}
