package metric

import (
	"bytes"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestMemoryReport(t *testing.T) {
	defer clearMetrics()

	var m memMetrics
	m.register("", "")
	assert.Equal(t, len(m.Alloc.vals), 0)
	m.readStats()
	assert.Equal(t, len(m.Alloc.vals), 1)
	assert.Equal(t, len(m.TotalAlloc.vals), 1)
	assert.Equal(t, len(m.Sys.vals), 1)
}

func TestMemoryTwice(t *testing.T) {
	defer clearMetrics()

	// Set the debug log func
	var buf bytes.Buffer
	defer setTempDbgLog(&buf)()
	Memory(24*time.Hour, "", "")
	time.Sleep(100 * time.Millisecond)

	Memory(24*time.Hour, "", "")
	time.Sleep(100 * time.Millisecond)
	got := buf.String()
	exp := "metric: collect goroutine started\n"
	if got != exp {
		t.Errorf("expected %s, got %s", exp, got)
	}
}
