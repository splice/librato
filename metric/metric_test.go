package metric

import (
	"os"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestMetric(t *testing.T) {
	defer clearMetrics()

	// Create new metric
	m := New(Counter, "test", "", "", "test")
	assert.Equal(t, m.Name, "test")
	host, err := os.Hostname()
	if err == nil {
		assert.Equal(t, m.Source, "test-"+host)
	} else {
		assert.Equal(t, m.Source, "")
	}

	// Add values to the metric
	m.Add(5.0)
	assert.Equal(t, m.last, 5.0)
	assert.Equal(t, len(m.vals), 1)
	// Two values may be added at the same key if done in the same second
	// (the key is the Unix timestamp, in secs), so sleep a second.
	time.Sleep(time.Second)
	// Add another value
	m.Add(2)
	assert.Equal(t, m.last, 7.0)
	assert.Equal(t, len(m.vals), 2)
	// Set an explicit value
	time.Sleep(time.Second)
	m.Set(3)
	assert.Equal(t, m.last, 3.0)
	assert.Equal(t, len(m.vals), 3)
	// Add a negative value
	time.Sleep(time.Second)
	m.Add(-2)
	assert.Equal(t, m.last, 1.0)
	assert.Equal(t, len(m.vals), 4)

	// Call flatten, moves vals to oldVals
	m.flatten()

	assert.Equal(t, m.last, 1.0)
	assert.Equal(t, len(m.vals), 0)
	assert.Equal(t, len(m.oldVals), 4)
	m.Add(3.0)
	assert.Equal(t, m.last, 4.0)
	assert.Equal(t, len(m.vals), 1)
	assert.Equal(t, len(m.oldVals), 4)

	// Call flatten, should replace oldVals
	m.flatten()
	assert.Equal(t, m.last, 4.0)
	assert.Equal(t, len(m.vals), 0)
	assert.Equal(t, len(m.oldVals), 1)

	// Call clear, should clear oldVals
	m.clear()
	assert.Equal(t, m.last, 4.0)
	assert.Equal(t, len(m.vals), 0)
	assert.Equal(t, len(m.oldVals), 0)
}

func TestRegisterMetric(t *testing.T) {
	defer clearMetrics()

	m := &Metric{Name: "test-lit"}

	// Metric is not registered
	_, ok := metrics[m.Name]
	assert.False(t, ok)

	Register(m)
	_, ok = metrics[m.Name]
	assert.True(t, ok)

	// Subsequent register panics
	assert.Panics(t, func() {
		Register(m)
	})
}

func TestDoMetrics(t *testing.T) {
	defer clearMetrics()

	// Create some metrics
	m1, m2, m3 := New(Counter, "t1", "", "", "test"), New(Gauge, "t2", "", "", "test"), New(Counter, "t3", "", "", "test")
	cnt := 0
	Do(func(m *Metric) {
		cnt++
		if m != m1 && m != m2 && m != m3 {
			t.Errorf("unexpected metric %s", m.Name)
		}
	})
	assert.Equal(t, cnt, 3)
}

func TestMetricReportedOnce(t *testing.T) {
	defer clearMetrics()

	m := New(Gauge, "test-reported-once", "display", "desc", "test")
	m.Add(34.55)
	mvs := m.flatten()
	if assert.Equal(t, len(mvs), 1) {
		assert.Equal(t, mvs[0].DisplayName, "display")
		assert.Equal(t, mvs[0].Description, "desc")
		m.clear()
		m.Add(23)
		mvs = m.flatten()
		if assert.Equal(t, len(mvs), 1) {
			assert.Equal(t, mvs[0].DisplayName, "")
			assert.Equal(t, mvs[0].Description, "")
		}
	}
}

func TestMetricName(t *testing.T) {
	defer clearMetrics()

	cases := []struct {
		in, out string
	}{
		0: {"", ""},
		1: {"a", "a"},
		2: {"#", "-"},
		3: {"abcDEFgHiJkLMNoPQRSTuvWXyZ0123456789-_.:?", "abcDEFgHiJkLMNoPQRSTuvWXyZ0123456789-_.:-"},
		4: {strings.Repeat("a", maxName), strings.Repeat("a", maxName)},
		5: {strings.Repeat("b", maxName+1), strings.Repeat("b", maxName)},
	}
	for i, c := range cases {
		got := normalize(c.in)
		if got != c.out {
			t.Errorf("%d: expected '%s' (%d), got '%s' (%d)", i, c.out, len(c.out), got, len(got))
		}
	}
}

func TestMetricGaugeAggregates(t *testing.T) {
	defer clearMetrics()

	m := New(Gauge, "test", "", "", "test")
	m.Add(2)
	mv := m.flatten()
	// A single value, should not set the gauges-specific parameters
	if assert.Equal(t, len(mv), 1) {
		assert.Equal(t, *mv[0].Value, 2.0)
		assert.Equal(t, mv[0].Count, 0)
		assert.Equal(t, mv[0].Sum, 0)
	}
	m.clear()

	now := time.Now().Unix()
	testForceKey = now
	m.Add(2)
	testForceKey = now
	m.Add(3)
	testForceKey = now
	m.Add(4)
	mv = m.flatten()
	if assert.Equal(t, len(mv), 1) {
		assert.Nil(t, mv[0].Value)
		assert.Equal(t, mv[0].Count, 3)
		assert.Equal(t, mv[0].Max, 11.0)
		assert.Equal(t, mv[0].Min, 4.0)
		assert.Equal(t, mv[0].Sum, 22.0)
		assert.Equal(t, mv[0].SumSquares, 186.0)
	}
}
