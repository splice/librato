// Package librato provides a simple API to report metrics to the Librato service.
//
// The API is similar to Go's expvar package. Create a Metric and call Add(delta)
// or Set(value) whenever there is some value to report. Metrics are multithread-safe
// and currently support Counters and Gauges. A goroutine runs in the background and
// reports the metrics at a given interval (currently, this interval - in
// librato/metric.ReportPeriod, cannot be changed during execution). The function
// librato/metric.StartReporting starts the reporting goroutine (guaranteed to start only one).
//
// Some nice features:
//
// * automatic retries in case of 503 (configurable number of retries and retry delay)
// * automatic splitting of POST reporting requests based on the Librato-recommended
// number of 300 metrics per call (configurable)
// * normalization of metrics' name and source fields
// * automatic removal of DisplayName, Description, and other metadata on each call but
// the first (so the payload is as small as possible, and a new Metric gets created with
// the correct metadata on first call)
// * values get two chances of being reported - if a post to librato fails for whatever
// reason, the same values (along with new ones) will be sent again on the next attempt,
// but then will be flushed if the post still fails (so that the payload doesn't keep on
// growing indefinitely). New values for this second call get another chance, and so on.
// * the package takes care of computing the min, max, count, sum and sumsquare aggregates
// for gauges.
// * one simple call to register all runtime memory statistics as Librato metrics.
//
// Some less-nice features that we didn't get the chance to fix at the moment:
//
// * Metric struct should not be exposed, makes no sense to use literal notation, internal
// fields must be created.
// * New should probably return an interface, and we could have gauge- and counter- specific
// implementations.
// * Since New automatically registers, Register is not required, nor NormalizeXx.
// * Maybe give more control over MemStats registration.
//
package librato
