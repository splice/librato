// This package's implementation and API is inspired by https://github.com/bmizerany/aws4.

package librato

import (
	"io"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"
)

// DefaultClient is the default client instance used by package-level functions.
// It doesn't retry on 503s by default.
var DefaultClient = &Client{
	Credentials: CredentialsFromEnv(),
}

// Credentials holds a set of Librato Credentials.
type Credentials struct {
	User  string
	Token string
}

// HTTPClient defines the only method required to act as an http.Client for this package's
// purpose. This is mostly to enable mocking of the *http.Client instance of the
// librato.Client.
type HTTPClient interface {
	Do(*http.Request) (*http.Response, error)
}

// Client is like http.Client, but authenticates all requests with Credentials.
type Client struct {
	// Credentials to use to authenticate with Librato's API.
	Credentials *Credentials

	// Retries indicates the number of times to retry the same request if Librato
	// responds with a status code 503. Zero means no retry.
	Retries int

	// RetryDelay indicates the time to wait before a retry. Zero means retry immediately.
	RetryDelay time.Duration

	// The http client to make requests with. If nil, http.DefaultClient is used.
	Client HTTPClient
}

// CredentialsFromEnv initializes and returns a Credentials using the LIBRATO_USER and LIBRATO_TOKEN
// environment variables.
func CredentialsFromEnv() *Credentials {
	return &Credentials{
		User:  os.Getenv("LIBRATO_USER"),
		Token: os.Getenv("LIBRATO_TOKEN"),
	}
}

// Get works like http.Get, but authenticates the request with Credentials.
func Get(url string) (resp *http.Response, err error) {
	return DefaultClient.Get(url)
}

// Head works like http.Head, but authenticates the request with Credentials.
func Head(url string) (resp *http.Response, err error) {
	return DefaultClient.Head(url)
}

// Post works like http.Post, but authenticates the request with Credentials.
func Post(url string, bodyType string, body io.Reader) (resp *http.Response, err error) {
	return DefaultClient.Post(url, bodyType, body)
}

// PostForm works like http.PostForm, but authenticates the request with Credentials.
func PostForm(url string, data url.Values) (resp *http.Response, err error) {
	return DefaultClient.PostForm(url, data)
}

// client returns the HTTPClient instance to use.
func (c *Client) client() HTTPClient {
	if c.Client == nil {
		return http.DefaultClient
	}
	return c.Client
}

// doHandleRetry executes the request and handles the retries as configured on the
// Client if the Librato API endpoint returns a status code 503.
func (c *Client) doHandleRetry(req *http.Request) (res *http.Response, err error) {
	cli := c.client()
	// Note: <= because must run initial request, then number of retries.
	for i := 0; i <= c.Retries; i++ {
		// If this is a retry and a delay is specified, wait.
		if i > 0 && c.RetryDelay > 0 {
			time.Sleep(c.RetryDelay)
		}
		// Do the request.
		res, err = cli.Do(req)
		if err != nil || res.StatusCode != 503 {
			break
		}
	}
	return res, err
}

// Do executes the specified request with authentication info from Credentials.
func (c *Client) Do(req *http.Request) (resp *http.Response, err error) {
	Auth(c.Credentials, req)
	// Ensure connection is closed, since requests are made at intervals likely to be
	// greater than keep-alive timeout anyway. This is to avoid the EOF error on Post.
	//
	// See:
	// http://stackoverflow.com/questions/17714494/golang-http-request-results-in-eof-errors-when-making-multiple-requests-successi
	req.Close = true
	return c.doHandleRetry(req)
}

// Get executes the GET request with authentication info from Credentials.
func (c *Client) Get(url string) (resp *http.Response, err error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	return c.Do(req)
}

// Head executes the specified HEAD request with authentication info from Credentials.
func (c *Client) Head(url string) (resp *http.Response, err error) {
	req, err := http.NewRequest("HEAD", url, nil)
	if err != nil {
		return nil, err
	}
	return c.Do(req)
}

// Post executes the specified POST request with authentication info from Credentials.
func (c *Client) Post(url string, bodyType string, body io.Reader) (resp *http.Response, err error) {
	req, err := http.NewRequest("POST", url, body)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", bodyType)
	return c.Do(req)
}

// PostForm executes the specified POST request with the provided form values, with authentication
// info from Credentials.
func (c *Client) PostForm(url string, data url.Values) (resp *http.Response, err error) {
	return c.Post(url, "application/x-www-form-urlencoded", strings.NewReader(data.Encode()))
}

// Auth adds authentication info provided by c to the request r.
func Auth(c *Credentials, r *http.Request) {
	if c == nil || r == nil {
		return
	}
	r.SetBasicAuth(c.User, c.Token)
}
